package com.codfidea.dmmt.listener;

public interface SmsListener {
    public void onMessageReceived(String messageText);
}
