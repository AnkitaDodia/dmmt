package com.codfidea.dmmt.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ComplaintsListData {

    @SerializedName("product_name")
    @Expose
    private String productName;
    @SerializedName("id_complaints")
    @Expose
    private String idComplaints;
    @SerializedName("id_user")
    @Expose
    private String idUser;
    @SerializedName("id_product")
    @Expose
    private String idProduct;
    @SerializedName("common_complaints")
    @Expose
    private String commonComplaints;
    @SerializedName("extra_complaints")
    @Expose
    private String extraComplaints;
    @SerializedName("machine_number")
    @Expose
    private String machineNumber;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("company")
    @Expose
    private String company;
    @SerializedName("id_display")
    @Expose
    private String idDisplay;

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getIdComplaints() {
        return idComplaints;
    }

    public void setIdComplaints(String idComplaints) {
        this.idComplaints = idComplaints;
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public String getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(String idProduct) {
        this.idProduct = idProduct;
    }

    public String getCommonComplaints() {
        return commonComplaints;
    }

    public void setCommonComplaints(String commonComplaints) {
        this.commonComplaints = commonComplaints;
    }

    public String getExtraComplaints() {
        return extraComplaints;
    }

    public void setExtraComplaints(String extraComplaints) {
        this.extraComplaints = extraComplaints;
    }

    public String getMachineNumber() {
        return machineNumber;
    }

    public void setMachineNumber(String machineNumber) {
        this.machineNumber = machineNumber;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getIdDisplay() {
        return idDisplay;
    }

    public void setIdDisplay(String idDisplay) {
        this.idDisplay = idDisplay;
    }

}
