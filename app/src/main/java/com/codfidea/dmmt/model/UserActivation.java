package com.codfidea.dmmt.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserActivation {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("user_status")
    @Expose
    private String userStatus;
    @SerializedName("data")
    @Expose
    private Object data;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}