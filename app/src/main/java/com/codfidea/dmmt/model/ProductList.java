package com.codfidea.dmmt.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ProductList
{
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("user_status")
    @Expose
    private String userStatus;
    @SerializedName("data")
    @Expose
    private ArrayList<ProductListData> data = null;
    @SerializedName("message")
    @Expose
    private String message;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }

    public ArrayList<ProductListData> getData() {
        return data;
    }

    public void setData(ArrayList<ProductListData> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
