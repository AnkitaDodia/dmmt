package com.codfidea.dmmt.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatTextView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.codfidea.dmmt.R;
import com.codfidea.dmmt.common.BaseActivity;
import com.codfidea.dmmt.model.UserDetails;
import com.codfidea.dmmt.model.UserValidationFromServer;
import com.codfidea.dmmt.restinterface.RestInterface;
import com.google.gson.Gson;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.text.Spanned.SPAN_EXCLUSIVE_INCLUSIVE;

public class SplashActivity extends BaseActivity {

    private static final String TAG = "SplashActivity";
    Context mContext;

    private static int SPLASH_TIME_OUT = 3000;
    AppCompatTextView mSpannable;
    

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        mContext = this;

        mSpannable = findViewById(R.id.textView3);

        SpannableStringBuilder spannable = new SpannableStringBuilder("DESIGN & DEVELOPED BY CODFIDEA");
        ForegroundColorSpan blue = new ForegroundColorSpan(getResources().getColor(R.color.span_text_color));
        spannable.setSpan(
                blue,
                22, // start
                30, // end
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        );

        mSpannable.setText(spannable);
        
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {

                if(getLogin() == 0)
                {
                    startActivity(new Intent(mContext, LoginActivity.class));
                    finish();
                }else {
                    if (BaseActivity.CheckInternet(mContext)) {
                        sendUserValidationRequest();
                    } else {
                        Toast.makeText(mContext, "કોઈ ઇન્ટરનેટ કનેક્શન મળ્યું નથી.", Toast.LENGTH_SHORT).show();
                    }
                }

            }
        }, SPLASH_TIME_OUT);
    }

    private void sendUserValidationRequest() {

        showWaitIndicator(true);

        UserDetails mUserDetails = getUserDetails(mContext);

        Log.e(TAG,"IdUser : "+mUserDetails.getIdUser());

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RestInterface.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestInterface service = retrofit.create(RestInterface.class);

        Call<UserValidationFromServer> call = service.sendUserValidationRequest(mUserDetails.getIdUser());

        call.enqueue(new Callback<UserValidationFromServer>() {
            @Override
            public void onResponse(Call<UserValidationFromServer> call, Response<UserValidationFromServer> response) {

                showWaitIndicator(false);

                try {
                    if (response.code() == 200) {

                        Log.e("USER_VALIDATION", "FULL RESPONSE : "+ new Gson().toJson(response.body().getData()));

                        if(response.body().getStatus() == 1){
                            startActivity(new Intent(mContext, HomeActivity.class));
                            finish();
                        }else {
                            startActivity(new Intent(mContext, LoginActivity.class));
                            finish();
                        }
                    }

                } catch (Exception e) {
//                    Log.d("onResponse", "There is an error");
                    showWaitIndicator(false);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<UserValidationFromServer> call, Throwable t) {
                showWaitIndicator(false);
                Log.d("onFailure", t.toString());
                Toast.makeText(mContext," "+mContext.getString(R.string.network_calling_error_msg), Toast.LENGTH_SHORT).show();

            }
        });
    }
}
