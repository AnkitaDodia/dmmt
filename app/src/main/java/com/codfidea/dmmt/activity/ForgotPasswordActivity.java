package com.codfidea.dmmt.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.codfidea.dmmt.R;
import com.codfidea.dmmt.common.BaseActivity;
import com.codfidea.dmmt.model.MessageModel;
import com.codfidea.dmmt.restinterface.RestInterface;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ForgotPasswordActivity extends BaseActivity
{
    Context mContext;
    LinearLayout ll_back_forgot,ll_parent_forgot_password;

    TextInputEditText edt_forgot_phone;

    Button btn_forgot_submit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_forgot_password);

        mContext = this;

        initViews();
        setClickListener();
    }

    private void initViews() {
        ll_parent_forgot_password = findViewById(R.id.ll_parent_forgot_password);
        overrideFonts(ll_parent_forgot_password,ForgotPasswordActivity.this);

        ll_back_forgot = findViewById(R.id.ll_back_forgot);

        edt_forgot_phone = findViewById(R.id.edt_forgot_phone);

        btn_forgot_submit = findViewById(R.id.btn_forgot_submit);
    }

    private void setClickListener()
    {
        ll_back_forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btn_forgot_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (TextUtils.isEmpty(edt_forgot_phone.getText().toString().trim())) {
//                    edt_login_email.setError("Please enter email");
                    Toast.makeText(mContext, "ફોન નંબર એન્ટર કરો.", Toast.LENGTH_LONG).show();
                } else {
                    if (CheckInternet(mContext)) {
                        sendMatchOTPRequest();
                    } else {
                        Toast.makeText(mContext, "કોઈ ઇન્ટરનેટ કનેક્શન મળ્યું નથી.", Toast.LENGTH_SHORT).show();
                    }
                }

            }
        });
    }

    private void sendMatchOTPRequest() {

        showWaitIndicator(true);

        final String PHONE = edt_forgot_phone.getText().toString();

        Log.e("FORGOT", "PHONE : " + PHONE);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RestInterface.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestInterface service = retrofit.create(RestInterface.class);

        Call<MessageModel> call = service.sendForgotPasswordRequest(PHONE);

        call.enqueue(new Callback<MessageModel>() {
            @Override
            public void onResponse(Call<MessageModel> call, Response<MessageModel> response) {

                showWaitIndicator(false);

                try {
                    if (response.code() == 200) {

                        Toast.makeText(mContext, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();

                        if (response.body().getStatus() == 1) {


                            Toast.makeText(mContext, " " + response.body().getMessage(), Toast.LENGTH_SHORT).show();

                            Intent i = new Intent(mContext, VerificationActivity.class);
                            i.putExtra("phone", PHONE);
                            i.putExtra("isFrom", false);

                            startActivity(i);
                            finish();

                        } else {

                        }
                    }
                } catch (Exception e) {
                    showWaitIndicator(false);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<MessageModel> call, Throwable t) {
                showWaitIndicator(false);
                Log.e("FORGOT", "onFailure" + t.toString());
                Toast.makeText(mContext, " " + mContext.getString(R.string.network_calling_error_msg), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
