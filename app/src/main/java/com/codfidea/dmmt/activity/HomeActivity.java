package com.codfidea.dmmt.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.codfidea.dmmt.R;
import com.codfidea.dmmt.common.BaseActivity;
import com.codfidea.dmmt.fragment.AboutUsFragment;
import com.codfidea.dmmt.fragment.ComplaintsFragment;
import com.codfidea.dmmt.fragment.ContactUsFragment;
import com.codfidea.dmmt.fragment.EditProfileFragment;
import com.codfidea.dmmt.fragment.FeedBackFragment;
import com.codfidea.dmmt.fragment.ProductFragment;
import com.codfidea.dmmt.model.MessageModel;
import com.codfidea.dmmt.model.UserDetails;
import com.codfidea.dmmt.restinterface.RestInterface;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class HomeActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    final String TAG = "HOME_ACTIVITY";
    Context mContext;
    Fragment currentFragment;
    private TextView mTitle;
    private Toolbar toolbar;
    NavigationView navigationView;
    boolean doubleBackToExitPressedOnce = false;
    DrawerLayout drawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nav_menu);

        mContext = this;

        InitViews();

    }

    private void InitViews() {

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.getMenu().getItem(0).setChecked(true);

        mTitle.setText(getResources().getString(R.string.nav_menu_products));
        changeFragment(new ProductFragment());

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer.openDrawer(Gravity.LEFT);
            }
        });

        drawer.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(@NonNull View drawerView, float slideOffset) {
                // Whatever you want
            }

            @Override
            public void onDrawerOpened(@NonNull View drawerView) {
                // Whatever you want
                BaseActivity.hideKeyboard(HomeActivity.this);
            }

            @Override
            public void onDrawerClosed(@NonNull View drawerView) {
                // Whatever you want
                BaseActivity.hideKeyboard(HomeActivity.this);
            }

            @Override
            public void onDrawerStateChanged(int newState) {
                // Whatever you want
            }
        });
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

//        UserDetails mUserDetails = getUserDetails(mContext);
        BaseActivity.hideKeyboard(HomeActivity.this);

        if (id == R.id.nav_product) {

            mTitle.setText(getResources().getString(R.string.nav_menu_products));
            changeFragment(new ProductFragment());
        }
        if (id == R.id.nav_complaints) {

            mTitle.setText(getResources().getString(R.string.nav_menu_complaint));
            changeFragment(new ComplaintsFragment());

            /*if(getUserStatus() == 1){



            }else {
                showNoComplaintsAlert();
            }*/

        } else if (id == R.id.nav_feedback) {

            if(getUserStatus() == 1){

                mTitle.setText(getResources().getString(R.string.nav_menu_complains));
                changeFragment(new FeedBackFragment());

            }else {
                showNoFeedBackAlert();
            }


        } else if (id == R.id.nav_contactus) {

            mTitle.setText(getResources().getString(R.string.nav_menu_contactus));
            changeFragment(new ContactUsFragment());

        } else if (id == R.id.nav_about) {

//            mTitle.setText(getResources().getString(R.string.nav_menu_aboutus));
//            changeFragment(new AboutUsFragment());

            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://dmtlaser.com/about-us.html")));

        } else if (id == R.id.nav_edit_profile) {

            mTitle.setText(getResources().getString(R.string.nav_menu_profile));
            changeFragment(new EditProfileFragment());

        } else if (id == R.id.nav_logout) {

            if (CheckInternet(mContext)) {
                sendLogoutRequest();
            } else {
                Toast.makeText(mContext, "કોઈ ઇન્ટરનેટ કનેક્શન મળ્યું નથી.", Toast.LENGTH_SHORT).show();
            }

        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void changeFragment(Fragment targetFragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.fragment_container, targetFragment, targetFragment.getClass().getName());
        transaction.setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        transaction.addToBackStack(targetFragment.getClass().getName());
        transaction.commitAllowingStateLoss();

        currentFragment = targetFragment;
    }

    private void sendLogoutRequest() {

        showWaitIndicator(true);
//        BaseActivity.FCMTOKEN = mContext.getFCMTOKEN();


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RestInterface.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestInterface service = retrofit.create(RestInterface.class);

        UserDetails mUserDetails = getUserDetails(mContext);

        Log.e("LOGOUT", "IdUser  " + mUserDetails.getIdUser());

        Call<MessageModel> call = service.sendLogoutRequest(mUserDetails.getIdUser());

        call.enqueue(new Callback<MessageModel>() {
            @Override
            public void onResponse(Call<MessageModel> call, Response<MessageModel> response) {

                showWaitIndicator(false);

                try {
                    if (response.code() == 200) {

//                        Log.e("LOGIN", "FULL RESPONSE : "+ new Gson().toJson(response.body().getData()));
                        Toast.makeText(mContext, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();

                        if (response.body().getStatus() == 1) {

                            ClearUserDetails(mContext);
                            setLogin(0);
                            Intent i = new Intent(mContext, LoginActivity.class);
                            startActivity(i);
                            finish();

                        } else {

                        }
                    }
                } catch (Exception e) {
//                    Log.d("onResponse", "There is an error");
                    showWaitIndicator(false);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<MessageModel> call, Throwable t) {
                showWaitIndicator(false);
                Log.e(TAG, "onFailure" + t.toString());
                Toast.makeText(mContext, " " + mContext.getString(R.string.network_calling_error_msg), Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public void onBackPressed() {
        BaseActivity.hideKeyboard(HomeActivity.this);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            performBackTransaction();
        }
    }

    public void performBackTransaction() {
        FragmentManager fm = getSupportFragmentManager();

        try {
            if (fm.getBackStackEntryCount() > 1) {
//                fm.popBackStackImmediate();

                String tag = fm.getBackStackEntryAt(fm.getBackStackEntryCount() - 1).getName();
                Log.e("MainActivity", "Current fragment tag is: " + tag);

                findTheFragmentWhilePoping(tag);
            } else {
                Log.i("MainActivity", "Nothing in back stack call super");
                if (doubleBackToExitPressedOnce) {
                    super.onBackPressed();

                    Intent intent = new Intent(Intent.ACTION_MAIN);
                    intent.addCategory(Intent.CATEGORY_HOME);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                            | Intent.FLAG_ACTIVITY_CLEAR_TASK
                            | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                    System.exit(0);
                    return;
                }

                this.doubleBackToExitPressedOnce = true;
                Toast.makeText(mContext, "Please click BACK again to exit", Toast.LENGTH_LONG).show();

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        doubleBackToExitPressedOnce = false;
                    }
                }, 2000);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void findTheFragmentWhilePoping(String tag) {
        FragmentManager fm = getSupportFragmentManager();

        if (tag.equalsIgnoreCase(ProductFragment.class.getName())) {

            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();

                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                        | Intent.FLAG_ACTIVITY_CLEAR_TASK
                        | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);

                finish();
                System.exit(0);
                return;
            }

            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(mContext, "Please click BACK again to exit", Toast.LENGTH_LONG).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);

        }else if (tag.equalsIgnoreCase(ComplaintsFragment.class.getName())) {

            navigationView.getMenu().getItem(0).setChecked(true);
            mTitle.setText(getResources().getString(R.string.nav_menu_products));
            currentFragment = new ProductFragment();

        }else if (tag.equalsIgnoreCase(FeedBackFragment.class.getName())) {

            navigationView.getMenu().getItem(0).setChecked(true);
            mTitle.setText(getResources().getString(R.string.nav_menu_products));
            currentFragment = new ProductFragment();

        }else if (tag.equalsIgnoreCase(EditProfileFragment.class.getName())) {

            navigationView.getMenu().getItem(0).setChecked(true);
            mTitle.setText(getResources().getString(R.string.nav_menu_products));
            currentFragment = new ProductFragment();

        }else if (tag.equalsIgnoreCase(ContactUsFragment.class.getName())) {

            navigationView.getMenu().getItem(0).setChecked(true);
            mTitle.setText(getResources().getString(R.string.nav_menu_products));
            currentFragment = new ProductFragment();

        }else if (tag.equalsIgnoreCase(AboutUsFragment.class.getName())) {

            navigationView.getMenu().getItem(0).setChecked(true);
            mTitle.setText(getResources().getString(R.string.nav_menu_products));
            currentFragment = new ProductFragment();

        }
        changeFragment(currentFragment);
    }

    public void backtoproduct(){

        navigationView.getMenu().getItem(0).setChecked(true);
        mTitle.setText(getResources().getString(R.string.nav_menu_products));
        currentFragment = new ProductFragment();
        changeFragment(currentFragment);
    }

    private void showNoComplaintsAlert() {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(mContext);
        builder.setTitle("માફ કરજો !")
                .setMessage("તમે કમ્પ્લેઇન નહિ કરી શકો, કમ્પ્લેઇનસ કરવા તમારા એકાઉન્ટ ને વેરીફાય કરવો.")
                .setPositiveButton("ઓકે", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        navigationView.getMenu().getItem(0).setChecked(true);
                        mTitle.setText(getResources().getString(R.string.nav_menu_products));
                        changeFragment(new ProductFragment());
                    }
                })
                .show();
    }

    private void showNoFeedBackAlert() {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(mContext);
        builder.setTitle("માફ કરજો !")
                .setMessage("તમે કમ્પ્લેઇન ને ફીડબેક નહિ આપી શકો, કમ્પ્લેઇન ને ફીડબેક કરવા તમારા એકાઉન્ટ ને વેરીફાય કરવો.")
                .setPositiveButton("ઓકે", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        navigationView.getMenu().getItem(0).setChecked(true);
                        mTitle.setText(getResources().getString(R.string.nav_menu_products));
                        changeFragment(new ProductFragment());
                    }
                })
                .show();
    }
}
