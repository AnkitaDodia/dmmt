package com.codfidea.dmmt.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.codfidea.dmmt.R;
import com.codfidea.dmmt.common.BaseActivity;
import com.codfidea.dmmt.model.Login;
import com.codfidea.dmmt.model.MessageModel;
import com.codfidea.dmmt.model.UserDetails;
import com.codfidea.dmmt.restinterface.RestInterface;
import com.google.gson.Gson;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SendInquiryActivity extends BaseActivity
{
    final String TAG = "INQUIRY";
    Context mContext;
    LinearLayout layout_send_inquiry_main,ll_back_send_inquiry;

    TextInputEditText edt_send_inquiry_fullname,edt_send_inquiry_machine_name,edt_send_inquiry_phone,
                      edt_send_inquiry_company_name,edt_send_inquiry_comment;

    Button btn_send_inquiry;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_inquiry);

        mContext = this;

        initView();
        setClickListener();
        setdata();
    }

    private void initView()
    {
        layout_send_inquiry_main = findViewById(R.id.layout_send_inquiry_main);
        overrideFonts(layout_send_inquiry_main,SendInquiryActivity.this);

        ll_back_send_inquiry = findViewById(R.id.ll_back_send_inquiry);

        btn_send_inquiry = findViewById(R.id.btn_send_inquiry);

        edt_send_inquiry_fullname = findViewById(R.id.edt_send_inquiry_fullname);
        edt_send_inquiry_machine_name = findViewById(R.id.edt_send_inquiry_machine_name);
        edt_send_inquiry_phone = findViewById(R.id.edt_send_inquiry_phone);
        edt_send_inquiry_company_name = findViewById(R.id.edt_send_inquiry_company_name);
        edt_send_inquiry_comment = findViewById(R.id.edt_send_inquiry_comment);

        edt_send_inquiry_fullname.setEnabled(false);
        edt_send_inquiry_machine_name.setEnabled(false);
        edt_send_inquiry_phone.setEnabled(false);
        edt_send_inquiry_company_name.setEnabled(false);
        edt_send_inquiry_comment.requestFocus();
    }

    private void setClickListener()
    {
        btn_send_inquiry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (TextUtils.isEmpty(edt_send_inquiry_comment.getText().toString().trim())) {
//                    edt_login_email.setError("Please enter email");
                    Toast.makeText(mContext,"પૂછપરછ મેસજ એન્ટર કરો.",Toast.LENGTH_LONG).show();
                } else {
                    if (CheckInternet(mContext)) {
                        sendInquiryRequest();
                    } else {
                        Toast.makeText(mContext, "કોઈ ઇન્ટરનેટ કનેક્શન મળ્યું નથી.", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        ll_back_send_inquiry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });
    }

    private void setdata() {

        UserDetails mUserDetails = getUserDetails(mContext);

//        mUserDetails.getIdUser()
        edt_send_inquiry_fullname.setText(mUserDetails.getFirstName());
        edt_send_inquiry_phone.setText(mUserDetails.getPhonenumber());
        edt_send_inquiry_company_name.setText(mUserDetails.getCompany());

        edt_send_inquiry_machine_name.setText(BaseActivity.INQUIRY_MACHINE_NAME);

        Log.e(TAG, "Id User  " +mUserDetails.getIdUser());
        Log.e(TAG, "Phone Number  " +mUserDetails.getPhonenumber());
        Log.e(TAG, "Company Name  " +mUserDetails.getCompany());
        Log.e(TAG, "Machine Name  " +BaseActivity.INQUIRY_MACHINE_NAME);
    }

    private void sendInquiryRequest() {

        showWaitIndicator(true);

        UserDetails mUserDetails = getUserDetails(mContext);

        String InquiryMessage = edt_send_inquiry_comment.getText().toString();

        Log.e(TAG,"IdUser : "+mUserDetails.getIdUser());
        Log.e(TAG,"Product Id : "+BaseActivity.INQUIRY_MACHINE_ID);
        Log.e(TAG,"Inquiry Message : "+InquiryMessage);


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RestInterface.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestInterface service = retrofit.create(RestInterface.class);

        Call<MessageModel> call = service.sendInquiryRequest(mUserDetails.getIdUser(), BaseActivity.INQUIRY_MACHINE_ID, InquiryMessage);

        call.enqueue(new Callback<MessageModel>() {
            @Override
            public void onResponse(Call<MessageModel> call, Response<MessageModel> response) {

                showWaitIndicator(false);

                try {
                    if (response.code() == 200) {

                        Toast.makeText(mContext, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();

                        if(response.body().getStatus() == 1){

                            showThankYouAlert();

                        }else {

                        }
                    }
                } catch (Exception e) {
//                    Log.d("onResponse", "There is an error");
                    showWaitIndicator(false);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<MessageModel> call, Throwable t) {
                showWaitIndicator(false);
                Log.e(TAG,"onFailure"+t.toString());
                Toast.makeText(mContext," "+mContext.getString(R.string.network_calling_error_msg), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void showThankYouAlert() {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(mContext);
        builder.setTitle("આભાર !")
                .setMessage("પૂછપરછ કરવા બદલ આભાર, થોડા સમય માં અમે તમારો સંપર્ક કરીશું.")
                .setPositiveButton("ઓકે", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        finish();
                    }
                })
//                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int which) {
//
//                    }
//                })
                .show();
    }
}
