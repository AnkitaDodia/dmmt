package com.codfidea.dmmt.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.codfidea.dmmt.R;
import com.codfidea.dmmt.broadcast.SMSBroadcastReceiver;
import com.codfidea.dmmt.common.BaseActivity;
import com.codfidea.dmmt.listener.SmsListener;
import com.codfidea.dmmt.model.MessageModel;
import com.codfidea.dmmt.restinterface.RestInterface;
import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class VerificationActivity extends BaseActivity {
    Context mContext;
    LinearLayout layout_verification_main;
    TextInputEditText edt_verify_code;
    Button btn_verify_submit;
    TextView text_resend_otp;

    private String TAG = "VERIFY";

    private String PHONE = null, UserId= null;
    private boolean isFromRegistration = true;

    private SmsRetrieverClient mSmsRetrieverClient;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_verification);

        mContext = this;

        initView();
        setClickListener();

        PHONE = getIntent().getStringExtra("phone");
        isFromRegistration = getIntent().getBooleanExtra("isFrom", true);
        UserId = getIntent().getStringExtra("userid");
        Log.e(TAG, "phone : " + PHONE);

        mSmsRetrieverClient = SmsRetriever.getClient(this);
        startVerify();

        SMSBroadcastReceiver.bindListener(new SmsListener() {
            @Override
            public void onMessageReceived(String messageText) {

                edt_verify_code.setText(messageText);
                if (TextUtils.isEmpty(edt_verify_code.getText().toString().trim())) {
//                    edt_login_email.setError("Please enter email");
                    Toast.makeText(mContext, "ઓ ટી પી  એન્ટર કરો.", Toast.LENGTH_LONG).show();
                } else {
                    if (CheckInternet(mContext)) {
                        if(isFromRegistration){
                            sendMatchOTPRequest();
                        }else {
                            sendForgotMatchOTPRequest();
                        }
                    } else {
                        Toast.makeText(mContext, "કોઈ ઇન્ટરનેટ કનેક્શન મળ્યું નથી.", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

    }

    private void initView() {
        layout_verification_main = findViewById(R.id.layout_verification_main);
        overrideFonts(layout_verification_main, VerificationActivity.this);

        edt_verify_code = findViewById(R.id.edt_verify_code);

        btn_verify_submit = findViewById(R.id.btn_verify_submit);

        text_resend_otp = findViewById(R.id.text_resend_otp);
    }

    private void setClickListener() {
        btn_verify_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (TextUtils.isEmpty(edt_verify_code.getText().toString().trim())) {
//                    edt_login_email.setError("Please enter email");
                    Toast.makeText(mContext, "ઓ ટી પી  એન્ટર કરો.", Toast.LENGTH_LONG).show();
                } else {
                    if (CheckInternet(mContext)) {
                        if(isFromRegistration){
                            sendMatchOTPRequest();
                        }else {
                            sendForgotMatchOTPRequest();
                        }
                    } else {
                        Toast.makeText(mContext, "કોઈ ઇન્ટરનેટ કનેક્શન મળ્યું નથી.", Toast.LENGTH_SHORT).show();
                    }
                }

            }
        });

        text_resend_otp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CheckInternet(mContext)) {
                    if(isFromRegistration){
                        sendReSendOTPRequest();
                    }else {
                        sendResendOtpMatchRequestForForgot();
                    }
                } else {
                    Toast.makeText(mContext, "કોઈ ઇન્ટરનેટ કનેક્શન મળ્યું નથી.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    private void startVerify() {
        // Make this a foreground service

        // Start SMS receiver code
        Task<Void> task = mSmsRetrieverClient.startSmsRetriever();
        task.addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {

                Log.e(TAG, "SmsRetrievalResult start onSuccess.");


            }
        });

        task.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.e(TAG, "SmsRetrievalResult start failed.", e);
            }
        });

    }

    //Code for match otp for registration
    private void sendMatchOTPRequest() {

        showWaitIndicator(true);

        String otp = edt_verify_code.getText().toString();

        Log.e(TAG, "OTP : " + otp);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RestInterface.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestInterface service = retrofit.create(RestInterface.class);

        Call<MessageModel> call = service.sendMatchOTPRequest(PHONE, otp);

        call.enqueue(new Callback<MessageModel>() {
            @Override
            public void onResponse(Call<MessageModel> call, Response<MessageModel> response) {

                showWaitIndicator(false);

                try {
                    if (response.code() == 200) {

                        Toast.makeText(mContext, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();

                        if (response.body().getStatus() == 1) {


                            Toast.makeText(mContext, " " + response.body().getMessage(), Toast.LENGTH_SHORT).show();

                            Intent i = new Intent(mContext, LoginActivity.class);
                            startActivity(i);
                            finish();

                        } else {

                        }
                    }
                } catch (Exception e) {
                    showWaitIndicator(false);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<MessageModel> call, Throwable t) {
                showWaitIndicator(false);
                Log.e(TAG, "onFailure" + t.toString());
                Toast.makeText(mContext, " " + mContext.getString(R.string.network_calling_error_msg), Toast.LENGTH_SHORT).show();
            }
        });
    }

    // Code for otp match request for forgot password
    private void sendForgotMatchOTPRequest() {

        showWaitIndicator(true);

        String otp = edt_verify_code.getText().toString();

        Log.e(TAG, "OTP : " + otp);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RestInterface.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestInterface service = retrofit.create(RestInterface.class);

        Call<MessageModel> call = service.sendForgotMatchOTPRequest(PHONE, otp);

        call.enqueue(new Callback<MessageModel>() {
            @Override
            public void onResponse(Call<MessageModel> call, Response<MessageModel> response) {

                showWaitIndicator(false);

                try {
                    if (response.code() == 200) {

                        Toast.makeText(mContext, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();

                        if (response.body().getStatus() == 1) {

                            Toast.makeText(mContext, " " + response.body().getMessage(), Toast.LENGTH_SHORT).show();

                            Intent i = new Intent(mContext, ResetPasswordActivity.class);
                            i.putExtra("phone", PHONE);
                            startActivity(i);
                            finish();

                        } else {

                        }
                    }
                } catch (Exception e) {
                    showWaitIndicator(false);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<MessageModel> call, Throwable t) {
                showWaitIndicator(false);
                Log.e(TAG, "onFailure" + t.toString());
                Toast.makeText(mContext, " " + mContext.getString(R.string.network_calling_error_msg), Toast.LENGTH_SHORT).show();
            }
        });
    }


    //Code for resend otp for registration
    private void sendReSendOTPRequest() {

        showWaitIndicator(true);

        String otp = edt_verify_code.getText().toString();

        Log.e(TAG, "OTP : " + otp);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RestInterface.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestInterface service = retrofit.create(RestInterface.class);

        Call<MessageModel> call = service.sendReSendOTPRequest(UserId,PHONE);

        call.enqueue(new Callback<MessageModel>() {
            @Override
            public void onResponse(Call<MessageModel> call, Response<MessageModel> response) {

                showWaitIndicator(false);

                try {
                    if (response.code() == 200) {

                        Toast.makeText(mContext, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();

                        if (response.body().getStatus() == 1) {

                            Toast.makeText(mContext, " " + response.body().getMessage(), Toast.LENGTH_SHORT).show();

                        } else {

                        }
                    }
                } catch (Exception e) {
                    showWaitIndicator(false);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<MessageModel> call, Throwable t) {
                showWaitIndicator(false);
                Log.e(TAG, "onFailure" + t.toString());
                Toast.makeText(mContext, " " + mContext.getString(R.string.network_calling_error_msg), Toast.LENGTH_SHORT).show();
            }
        });
    }


    //Code for resend otp for forgot password
    private void sendResendOtpMatchRequestForForgot() {

        showWaitIndicator(true);

        Log.e("FORGOT", "PHONE : " + PHONE);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RestInterface.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestInterface service = retrofit.create(RestInterface.class);

        Call<MessageModel> call = service.sendForgotPasswordRequest(PHONE);

        call.enqueue(new Callback<MessageModel>() {
            @Override
            public void onResponse(Call<MessageModel> call, Response<MessageModel> response) {

                showWaitIndicator(false);

                try {
                    if (response.code() == 200) {

                        Toast.makeText(mContext, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();

                        if (response.body().getStatus() == 1) {
                            Toast.makeText(mContext, " " + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        } else {

                        }
                    }
                } catch (Exception e) {
                    showWaitIndicator(false);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<MessageModel> call, Throwable t) {
                showWaitIndicator(false);
                Log.e("FORGOT", "onFailure" + t.toString());
                Toast.makeText(mContext, " " + mContext.getString(R.string.network_calling_error_msg), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
