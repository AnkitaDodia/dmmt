package com.codfidea.dmmt.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.codfidea.dmmt.R;
import com.codfidea.dmmt.common.BaseActivity;
import com.codfidea.dmmt.model.MessageModel;
import com.codfidea.dmmt.model.ResetPassword;
import com.codfidea.dmmt.restinterface.RestInterface;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ResetPasswordActivity extends BaseActivity {
    ResetPasswordActivity mContext;

    LinearLayout ll_parent_reset_password;

    TextInputEditText edt_reset_pass,edt_reset_pass_confirm;

    Button btn_reset_pass_submit;

    private String TAG = "ResetPasswordActivity";

    private String phone = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);

        mContext = this;

        phone = getIntent().getStringExtra("phone");

        initView();
        setClickListener();
    }

    private void initView() {
        ll_parent_reset_password = findViewById(R.id.ll_parent_reset_password);
        overrideFonts(ll_parent_reset_password,mContext);

        edt_reset_pass = findViewById(R.id.edt_reset_pass);
        edt_reset_pass_confirm = findViewById(R.id.edt_reset_pass_confirm);

        btn_reset_pass_submit = findViewById(R.id.btn_reset_pass_submit);
    }

    private void setClickListener() {
        btn_reset_pass_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(edt_reset_pass.getText().toString().length() == 0){
                    Toast.makeText(mContext,"પાસવર્ડ એન્ટર કરો.",Toast.LENGTH_LONG).show();
                }else if(edt_reset_pass_confirm.getText().toString().length() == 0){
                    Toast.makeText(mContext,"કોન્ફીર્મ પાસવર્ડ એન્ટર કરો",Toast.LENGTH_LONG).show();
                }else if(!edt_reset_pass.getText().toString().equalsIgnoreCase(edt_reset_pass_confirm.getText().toString())){
                    Toast.makeText(mContext,"પાસવર્ડ અને કોન્ફીર્મ પાસવર્ડ મેચ નથી થતા",Toast.LENGTH_LONG).show();
                }else {
                    if (CheckInternet(mContext)) {
                        sendResetPasswordRequest();
                    } else {
                        Toast.makeText(mContext, "કોઈ ઇન્ટરનેટ કનેક્શન મળ્યું નથી.", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    private void sendResetPasswordRequest() {
        showWaitIndicator(true);

        String password = edt_reset_pass.getText().toString();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RestInterface.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestInterface service = retrofit.create(RestInterface.class);

        Call<ResetPassword> call = service.sendResetPasswordRequest(phone, password);

        call.enqueue(new Callback<ResetPassword>() {
            @Override
            public void onResponse(Call<ResetPassword> call, Response<ResetPassword> response) {

                showWaitIndicator(false);

                try {
                    if (response.code() == 200) {

                        Toast.makeText(mContext, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();

                        if (response.body().getStatus() == 1) {
                            Toast.makeText(mContext, " " + response.body().getMessage(), Toast.LENGTH_SHORT).show();

                            Intent i = new Intent(mContext, LoginActivity.class);
                            startActivity(i);
                            finish();

                        } else {

                        }
                    }
                } catch (Exception e) {
                    showWaitIndicator(false);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResetPassword> call, Throwable t) {
                showWaitIndicator(false);
                Log.e(TAG, "onFailure" + t.toString());
                Toast.makeText(mContext, " " + mContext.getString(R.string.network_calling_error_msg), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
