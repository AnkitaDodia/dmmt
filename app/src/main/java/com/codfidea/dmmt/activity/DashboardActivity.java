//package com.codfidea.dmmt.activity;
//
//import android.os.Bundle;
//import android.support.annotation.Nullable;
//import android.support.v7.widget.LinearLayoutManager;
//import android.support.v7.widget.RecyclerView;
//import android.util.Log;
//import android.view.View;
//import android.widget.LinearLayout;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.codfidea.dmmt.R;
//import com.codfidea.dmmt.adapter.ProductListAdapter;
//import com.codfidea.dmmt.common.BaseActivity;
//import com.codfidea.dmmt.model.ProductList;
//import com.codfidea.dmmt.model.ProductListData;
//import com.codfidea.dmmt.model.UserDetails;
//import com.codfidea.dmmt.restinterface.RestInterface;
//import com.google.gson.Gson;
//
//import java.util.ArrayList;
//
//import retrofit2.Call;
//import retrofit2.Callback;
//import retrofit2.Response;
//import retrofit2.Retrofit;
//import retrofit2.converter.gson.GsonConverterFactory;
//
//public class DashboardActivity extends BaseActivity
//{
//    DashboardActivity mContext;
//
//    RecyclerView rv_products;
//
//    TextView txt_empty_products;
//
//    LinearLayout layout_dashboard_main;
//
//    ArrayList<ProductListData> mProductDataList = new ArrayList<>();
//
//    @Override
//    protected void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//
//        setContentView(R.layout.activity_dashboard);
//
//        mContext = this;
//
//        initView();
//
//        if (BaseActivity.CheckInternet(mContext)) {
//            sendProductRequest();
//        } else {
//            Toast.makeText(mContext, "કોઈ ઇન્ટરનેટ કનેક્શન મળ્યું નથી.", Toast.LENGTH_SHORT).show();
//        }
//    }
//
//    private void initView()
//    {
//        rv_products = findViewById(R.id.rv_products);
//        rv_products.setHasFixedSize(true);
//        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
//        rv_products.setLayoutManager(mLayoutManager);
//
//
//        txt_empty_products = findViewById(R.id.txt_empty_products);
//
//        layout_dashboard_main = findViewById(R.id.layout_dashboard_main);
//        overrideFonts(layout_dashboard_main,DashboardActivity.this);
//    }
//
//    private void sendProductRequest()
//    {
//        mContext.showWaitIndicator(true);
//
//        UserDetails mUserDetails = getUserDetails(mContext);
//
//        Log.e("DASHBOARD","IdUser : "+mUserDetails.getIdUser());
//
//        Retrofit retrofit = new Retrofit.Builder()
//                .baseUrl(RestInterface.API_BASE_URL)
//                .addConverterFactory(GsonConverterFactory.create())
//                .build();
//
//        RestInterface service = retrofit.create(RestInterface.class);
//
//        Call<ProductList> call = service.sendProductListRequest(mUserDetails.getIdUser());
//
//        call.enqueue(new Callback<ProductList>() {
//            @Override
//            public void onResponse(Call<ProductList> call, Response<ProductList> response) {
//
//                mContext.showWaitIndicator(false);
//
//                try {
//                    if (response.code() == 200) {
//
//                        Log.e("PRODUCT_LIST", "FULL RESPONSE : "+ new Gson().toJson(response.body().getData()));
//
//                        if(response.body().getStatus() == 1){
//
//                            rv_products.setVisibility(View.VISIBLE);
//                            txt_empty_products.setVisibility(View.GONE);
//
//                            mProductDataList = response.body().getData();
//
//                            setAdapter(mProductDataList);
//
//                        }else {
//                            rv_products.setVisibility(View.GONE);
//                            txt_empty_products.setVisibility(View.VISIBLE);
//                            txt_empty_products.setText("No Product Found");
//                        }
//                    }
//                } catch (Exception e) {
//                    mContext.showWaitIndicator(false);
//                    e.printStackTrace();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<ProductList> call, Throwable t) {
//                mContext.showWaitIndicator(false);
//                Log.d("onFailure", t.toString());
//                Toast.makeText(mContext," "+mContext.getString(R.string.network_calling_error_msg), Toast.LENGTH_SHORT).show();
//            }
//        });
//    }
//
//    private void setAdapter(ArrayList<ProductListData> mProductDataList)
//    {
//        ProductListAdapter adapter = new ProductListAdapter(mContext,mProductDataList);
//        rv_products.setAdapter(adapter);
//    }
//}
