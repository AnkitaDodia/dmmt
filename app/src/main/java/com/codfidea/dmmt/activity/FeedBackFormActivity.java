package com.codfidea.dmmt.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.codfidea.dmmt.R;
import com.codfidea.dmmt.common.BaseActivity;
import com.codfidea.dmmt.model.MessageModel;
import com.codfidea.dmmt.model.UserDetails;
import com.codfidea.dmmt.restinterface.RestInterface;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class FeedBackFormActivity extends BaseActivity {

    final String TAG = "FEEDBACK";
    Context mContext;
    private TextView txt_machine_no, txt_machine_name, txt_complaints_id, txt_complaints_title;
    private TextInputEditText edt_actual_cause, edt_solution, edt_future_note, edt_engineer_name;
    private RatingBar rb_feebback;
    private Button btn_feedback;
    private float mRatingValue = 0;
    private LinearLayout ll_back_feedback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed_back_form);

        mContext = this;

        InitViews();
        SetClickListner();
        SetData();
    }

    private void SetData() {

        txt_machine_name.setText(BaseActivity.mComplaintsList.get(BaseActivity.COMPLAINT_POSITION).getProductName());
        txt_machine_no.setText(BaseActivity.mComplaintsList.get(BaseActivity.COMPLAINT_POSITION).getMachineNumber());
        txt_complaints_id.setText(BaseActivity.mComplaintsList.get(BaseActivity.COMPLAINT_POSITION).getIdDisplay());
        txt_complaints_title.setText(BaseActivity.mComplaintsList.get(BaseActivity.COMPLAINT_POSITION).getExtraComplaints());

    }

    private void InitViews() {

        ll_back_feedback = findViewById(R.id.ll_back_feedback);

        txt_machine_no = findViewById(R.id.txt_machine_no);
        txt_machine_name = findViewById(R.id.txt_machine_name);
        txt_complaints_id = findViewById(R.id.txt_complaints_id);
        txt_complaints_title = findViewById(R.id.txt_complaints_title);

        edt_engineer_name = findViewById(R.id.edt_engineer_name);
        edt_actual_cause = findViewById(R.id.edt_actual_cause);
        edt_solution = findViewById(R.id.edt_solution);
        edt_future_note = findViewById(R.id.edt_future_note);

        rb_feebback = findViewById(R.id.rb_feebback);
        btn_feedback = findViewById(R.id.btn_feedback);


    }

    private void SetClickListner() {

        ll_back_feedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        rb_feebback.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {

            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {

                mRatingValue = rating;
            }

        });


        btn_feedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (TextUtils.isEmpty(edt_engineer_name.getText().toString().trim())) {
//                    edt_login_email.setError("Please enter email");
                    Toast.makeText(mContext, "એન્જીનીર નામ એન્ટર કરો.", Toast.LENGTH_LONG).show();
                } else if (TextUtils.isEmpty(edt_actual_cause.getText().toString().trim())) {
//                    edt_login_email.setError("Please enter email");
                    Toast.makeText(mContext, "પ્રોબ્લેમ એન્ટર કરો.", Toast.LENGTH_LONG).show();
                } else if (TextUtils.isEmpty(edt_solution.getText().toString().trim())) {
//                    edt_login_email.setError("Please enter email");
                    Toast.makeText(mContext, "સોલ્યૂશન એન્ટર કરો.", Toast.LENGTH_LONG).show();
                } else if (TextUtils.isEmpty(edt_future_note.getText().toString().trim())) {
//                    edt_login_email.setError("Please enter email");
                    Toast.makeText(mContext, "કોમેન્ટ એન્ટર કરો.", Toast.LENGTH_LONG).show();
                } else {
                    if (BaseActivity.CheckInternet(mContext)) {
                        sendComplaintsRequest();
                    } else {
                        Toast.makeText(mContext, "કોઈ ઇન્ટરનેટ કનેક્શન મળ્યું નથી.", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

    }

    private void sendComplaintsRequest() {

        showWaitIndicator(true);

        UserDetails mUserDetails = getUserDetails(mContext);

//        BaseActivity.mComplaintsList.get(BaseActivity.COMPLAINT_POSITION).getProductName()

        String IdUser = mUserDetails.getIdUser();
        String IdComplaints = BaseActivity.mComplaintsList.get(BaseActivity.COMPLAINT_POSITION).getIdComplaints();

        String mActualCause = edt_actual_cause.getText().toString().trim();
        String mSolution = edt_solution.getText().toString().trim();
        String mFutureNote = edt_future_note.getText().toString().trim();
        String mEngineerName = edt_engineer_name.getText().toString().trim();

        Log.e(TAG, "IdUser : " + IdUser);
        Log.e(TAG, "IdComplaints : " + IdComplaints);
        Log.e(TAG, "mActualCause : " + mActualCause);
        Log.e(TAG, "mSolution : " + mSolution);
        Log.e(TAG, "mFutureNote : " + mFutureNote);
        Log.e(TAG, "mEngineerName : " + mEngineerName);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RestInterface.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        RestInterface service = retrofit.create(RestInterface.class);

        Call<MessageModel> call = service.sendFeedbackRequest(IdUser, IdComplaints, mEngineerName, mActualCause, mSolution, mFutureNote, mRatingValue);

        call.enqueue(new Callback<MessageModel>() {
            @Override
            public void onResponse(Call<MessageModel> call, Response<MessageModel> response) {

                showWaitIndicator(false);

                try {
                    if (response.code() == 200) {

                        Log.e(TAG, "RESPONSE : " + response.body().getMessage());
                        Toast.makeText(mContext, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();

                        if (response.body().getStatus() == 1) {

                            showThankYouAlert();

                        } else {

                        }
                    }
                } catch (Exception e) {
//                    Log.d("onResponse", "There is an error");
                    showWaitIndicator(false);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<MessageModel> call, Throwable t) {
                showWaitIndicator(false);
                Log.d(TAG, "onFailure" + t.toString());
                Toast.makeText(mContext, " " + mContext.getString(R.string.network_calling_error_msg), Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void showThankYouAlert() {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(mContext);
        builder.setTitle("આભાર !")
                .setMessage("ફીડબેક આપવા બદલ આભાર.")
                .setPositiveButton("ઓકે", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        startActivity(new Intent(mContext, HomeActivity.class));
                        finish();
                    }
                })
//                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int which) {
//
//                    }
//                })
                .show();
    }
}
