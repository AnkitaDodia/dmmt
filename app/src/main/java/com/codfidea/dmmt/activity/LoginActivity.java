package com.codfidea.dmmt.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.codfidea.dmmt.R;
import com.codfidea.dmmt.common.BaseActivity;
import com.codfidea.dmmt.model.Login;
import com.codfidea.dmmt.model.UserDetails;
import com.codfidea.dmmt.restinterface.RestInterface;
import com.google.gson.Gson;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LoginActivity extends BaseActivity
{
    Context mContext;
    final String TAG = "LOGIN";

    TextView txt_forgotten_password,text_signUp;
    TextInputEditText edt_login_phone,edt_login_password;
    Button btn_login;
    LinearLayout ll_parent_login;
    TextInputLayout layout_email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);

        mContext = this;

        initView();
        setListener();
    }

    private void initView()
    {
        ll_parent_login = findViewById(R.id.ll_parent_login);
        overrideFonts(ll_parent_login,LoginActivity.this);

        txt_forgotten_password = findViewById(R.id.txt_forgotten_password);
        text_signUp = findViewById(R.id.text_signUp);

        edt_login_phone = findViewById(R.id.edt_login_phone);
        edt_login_password = findViewById(R.id.edt_login_password);

        btn_login = findViewById(R.id.btn_login);
    }

    private void setListener()
    {
        txt_forgotten_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this,ForgotPasswordActivity.class));
            }
        });

        text_signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this,RegistrationActivity.class));
            }
        });

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (TextUtils.isEmpty(edt_login_phone.getText().toString().trim())) {
//                    edt_login_email.setError("Please enter email");
                    Toast.makeText(mContext,"ફોન નંબર એન્ટર કરો.",Toast.LENGTH_LONG).show();
                }
                else if (edt_login_phone.getText().toString().length() < 10) {
//                    edt_login_email.setError("Please enter valid email");
                    Toast.makeText(mContext,"સાચો ફોન નંબર એન્ટર કરો.",Toast.LENGTH_LONG).show();
                }
                else if (TextUtils.isEmpty(edt_login_password.getText().toString().trim())) {
//                    edt_login_password.setError("Please enter valid password");
                    Toast.makeText(mContext,"પાસવર્ડ એન્ટર કરો.",Toast.LENGTH_LONG).show();
                } else {
                    if (CheckInternet(mContext)) {
                        sendLoginRequest();
                    } else {
                        Toast.makeText(mContext, "કોઈ ઇન્ટરનેટ કનેક્શન મળ્યું નથી.", Toast.LENGTH_SHORT).show();
                    }
                }

            }
        });
    }

    private void sendLoginRequest() {

        showWaitIndicator(true);

        String phone = edt_login_phone.getText().toString();
        String password = edt_login_password.getText().toString();

        Log.e(TAG,"phone : "+phone);
        Log.e(TAG,"Password : "+password);
        Log.e(TAG,"FCM Token : "+BaseActivity.FCMTOKEN);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RestInterface.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestInterface service = retrofit.create(RestInterface.class);

        Call<Login> call = service.sendLoginRequest(phone, password, BaseActivity.FCMTOKEN);

        call.enqueue(new Callback<Login>() {
            @Override
            public void onResponse(Call<Login> call, Response<Login> response) {

                showWaitIndicator(false);

                try {
                    if (response.code() == 200) {

                        Log.e("LOGIN", "FULL RESPONSE : "+ new Gson().toJson(response.body().getData()));
                        Toast.makeText(mContext, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();

                        if(response.body().getStatus() == 1){

                            UserDetails mUserDetails = new UserDetails();

                            mUserDetails.setIdUser(response.body().getData().getIdUser());
                            mUserDetails.setFirstName(response.body().getData().getName());
                            mUserDetails.setAddress(response.body().getData().getAddress());
                            mUserDetails.setEmail(response.body().getData().getEmail());
                            mUserDetails.setCompany(response.body().getData().getCompany());
                            mUserDetails.setPassword(response.body().getData().getPassword());
                            mUserDetails.setPhonenumber(response.body().getData().getPhone());

                            if(response.body().getData().getActivated().equalsIgnoreCase("0")){
                                mUserDetails.setActivated(false);
                                setUserStatus(0);
                            }else{
                                mUserDetails.setActivated(true);
                                setUserStatus(1);
                            }


                            setUserDetails(mContext, mUserDetails);

                            setLogin(1);
                            Intent i = new Intent(mContext, HomeActivity.class);
                            startActivity(i);
                            finish();

                        }else {

                        }
                    }
                } catch (Exception e) {
//                    Log.d("onResponse", "There is an error");
                    showWaitIndicator(false);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<Login> call, Throwable t) {
                showWaitIndicator(false);
                Log.e(TAG,"onFailure"+t.toString());
                Toast.makeText(mContext," "+mContext.getString(R.string.network_calling_error_msg), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
