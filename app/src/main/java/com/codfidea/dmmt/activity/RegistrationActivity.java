package com.codfidea.dmmt.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.codfidea.dmmt.R;
import com.codfidea.dmmt.common.BaseActivity;
import com.codfidea.dmmt.model.SignUp;
import com.codfidea.dmmt.restinterface.RestInterface;
import com.codfidea.dmmt.view.PrefixEditText;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RegistrationActivity extends BaseActivity
{
    final String TAG = "REGISTRATION";
    Context mContext;
    LinearLayout ll_back_reg,ll_parent_register;
    TextInputEditText edt_reg_fullname, edt_reg_company_name,  edt_reg_email, edt_reg_password, edt_reg_address;
    Button btn_register;

    PrefixEditText edt_reg_phone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_registration);

        mContext = this;

        initViews();
        setClickListener();
    }

    private void initViews() {
        ll_back_reg = findViewById(R.id.ll_back_reg);
        ll_parent_register = findViewById(R.id.ll_parent_register);

        edt_reg_fullname = findViewById(R.id.edt_reg_fullname);
        edt_reg_company_name = findViewById(R.id.edt_reg_company_name);
        edt_reg_phone = findViewById(R.id.edt_reg_phone);
        edt_reg_email = findViewById(R.id.edt_reg_email);
        edt_reg_password = findViewById(R.id.edt_reg_password);
        edt_reg_address = findViewById(R.id.edt_reg_address);

        btn_register = findViewById(R.id.btn_register);

        overrideFonts(ll_parent_register,mContext);

//        edt_reg_fullname.setText("Hardik");
//        edt_reg_company_name.setText("Codfidea");
//        edt_reg_phone.setText("9662472875");
//        edt_reg_email.setText("hardz.kubavat@mail.com");
//        edt_reg_password.setText("123456");
//        edt_reg_address.setText("Lathi");
    }

    private void setClickListener() {
        ll_back_reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (TextUtils.isEmpty(edt_reg_fullname.getText().toString().trim())) {
//                    edt_login_email.setError("Please enter email");
                    Toast.makeText(mContext,"આખું નામ એન્ટર કરો.",Toast.LENGTH_LONG).show();
                } else if (TextUtils.isEmpty(edt_reg_company_name.getText().toString().trim())) {
//                    edt_login_email.setError("Please enter email");
                    Toast.makeText(mContext,"કંપની નું નામ એન્ટર કરો.",Toast.LENGTH_LONG).show();
                } else if (TextUtils.isEmpty(edt_reg_phone.getText().toString().trim())) {
//                    edt_login_email.setError("Please enter email");
                    Toast.makeText(mContext,"ફોન નંબર એન્ટર કરો.",Toast.LENGTH_LONG).show();
                }else if (edt_reg_phone.getText().toString().trim().length() < 10) {
//                    edt_login_email.setError("Please enter email");
                    Toast.makeText(mContext,"સાચો ફોન નંબર એન્ટર કરો.",Toast.LENGTH_LONG).show();
                }
//                else if (TextUtils.isEmpty(edt_reg_email.getText().toString().trim())) {
////                    edt_login_email.setError("Please enter email");
//                    Toast.makeText(mContext,"ઇમેઇલ એડ્રેસ એન્ટર કરો.",Toast.LENGTH_LONG).show();
//                } else if (!BaseActivity.isValidEmail(edt_reg_email.getText().toString())) {
////                    edt_login_email.setError("Please enter valid email");
//                    Toast.makeText(mContext,"સાચું ઇમેઇલ એડ્રેસ એન્ટર કરો.",Toast.LENGTH_LONG).show();
//                }
                else if (TextUtils.isEmpty(edt_reg_password.getText().toString().trim())) {
//                    edt_login_password.setError("Please enter valid password");
                    Toast.makeText(mContext,"પાસવર્ડ એન્ટર કરો.",Toast.LENGTH_LONG).show();
                } else if (TextUtils.isEmpty(edt_reg_address.getText().toString().trim())) {
//                    edt_login_password.setError("Please enter valid password");
                    Toast.makeText(mContext,"સરનામું એડ્રેસ એન્ટર કરો.",Toast.LENGTH_LONG).show();
                } else {
                    if (BaseActivity.CheckInternet(mContext)) {
                        sendSignUpRequest();
                    } else {
                        Toast.makeText(mContext, "કોઈ ઇન્ટરનેટ કનેક્શન મળ્યું નથી.", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    private void sendSignUpRequest() {

        showWaitIndicator(true);

        String fullname = edt_reg_fullname.getText().toString().trim();
        String company_name = edt_reg_company_name.getText().toString().trim();
        final String phone = edt_reg_phone.getText().toString().trim();
        String address = edt_reg_address.getText().toString().trim();
        String email = edt_reg_email.getText().toString().trim();
        String password = edt_reg_password.getText().toString().trim();

//        BaseActivity.FCMTOKEN = mContext.getFCMTOKEN();

        Log.e(TAG,"Email : "+email);
        Log.e(TAG,"Password : "+password);
//        Log.e(TAG,"FCM Token : "+BaseActivity.FCMTOKEN);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RestInterface.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestInterface service = retrofit.create(RestInterface.class);

        Call<SignUp> call = service.sendSignUpRequest(fullname, email, phone, password, company_name, address);

        call.enqueue(new Callback<SignUp>() {
            @Override
            public void onResponse(Call<SignUp> call, Response<SignUp> response) {

                showWaitIndicator(false);

                try {
                    if (response.code() == 200) {

                        Log.e(TAG,"RESPONSE : "+response.body().getMessage());
                        Toast.makeText(mContext, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();

                        if(response.body().getStatus() == 1){
                            Log.e(TAG,"RESPONSE : "+response.body().getData().getPhone());

                            Intent i = new Intent(mContext, VerificationActivity.class);
                            i.putExtra("phone", phone);
                            i.putExtra("isFrom", true);
                            i.putExtra("userid", response.body().getData().getIdUser());
                            startActivity(i);
                            finish();
                        }else {

                        }
                    }
                } catch (Exception e) {
//                    Log.d("onResponse", "There is an error");
                    showWaitIndicator(false);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<SignUp> call, Throwable t) {
                showWaitIndicator(false);
                Log.d(TAG,"onFailure"+t.toString());
                Toast.makeText(mContext," "+mContext.getString(R.string.network_calling_error_msg), Toast.LENGTH_SHORT).show();

            }
        });
    }
}
