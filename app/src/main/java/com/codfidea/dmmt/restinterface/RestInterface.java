package com.codfidea.dmmt.restinterface;


import com.codfidea.dmmt.model.AboutUs;
import com.codfidea.dmmt.model.ComplaintsList;
import com.codfidea.dmmt.model.Login;
import com.codfidea.dmmt.model.MessageModel;
import com.codfidea.dmmt.model.ProductList;
import com.codfidea.dmmt.model.ResetPassword;
import com.codfidea.dmmt.model.SignUp;
import com.codfidea.dmmt.model.UserActivation;
import com.codfidea.dmmt.model.UserValidationFromServer;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;


/**
 * Created by codfidea on 09/22/2016.
 */
public interface RestInterface {

    String API_BASE_URL = "http://codfidea.com/dmmt/";

    String LOGIN = "api/login";
    String SIGNUP = "api/SignUp";
    String LOGOUT = "api/logout";

    String MACHINE_INQUIRY = "api/SendInquiry";
    String EDITPROFILE = "api/editProfile";

    String PRODUCT_LIST = "api/ProductList";

    String ADD_COMPLAINTS = "api/AddComplaints";
    String GET_COMPLAINTS = "api/GetComplaint";
    String ABOUT_US = "api/AboutUs";
    String FEEDBACK = "api/SendFeedback";

    String MATCH_OTP = "api/MatchOtp";
    String FORGOT_PASSWORD = "api/ForgotPassword";
    String FORGOT_OTPMATCH = "api/ForgotOtp";
    String RESEND_OTP = "api/ResendOtp";

    String USER_VALIDATION_FROM_SERVER = "api/IsUserExist";
    String RESET_PASSWORD = "api/ResetPassword";
    String ACTIVE_USER = "api/IsUserActivated";

    @FormUrlEncoded
    @POST(LOGIN)
    Call<Login> sendLoginRequest(@Field("phone") String email, @Field("password") String password, @Field("fcm") String fcm);

    @FormUrlEncoded
    @POST(FORGOT_PASSWORD)
    Call<MessageModel> sendForgotPasswordRequest(@Field("phone") String phone);

    @FormUrlEncoded
    @POST(FORGOT_OTPMATCH)
    Call<MessageModel> sendForgotMatchOTPRequest(@Field("phone") String phone, @Field("otp") String otp);

    @FormUrlEncoded
    @POST(MATCH_OTP)
    Call<MessageModel> sendMatchOTPRequest(@Field("phone") String phone, @Field("otp") String otp);

    @FormUrlEncoded
    @POST(RESEND_OTP)
    Call<MessageModel> sendReSendOTPRequest(@Field("id_user") String id_user,@Field("phone") String phone);

    @FormUrlEncoded
    @POST(SIGNUP)
    Call<SignUp> sendSignUpRequest(
            @Field("name") String first_name,
            @Field("email") String email,
            @Field("phone") String phone,
            @Field("password") String password,
            @Field("company") String company,
            @Field("address") String address);

    @FormUrlEncoded
    @POST(EDITPROFILE)
    Call<SignUp> sendEditProfileRequest(
            @Field("name") String first_name,
            @Field("email") String email,
            @Field("phone") String phone,
            @Field("company") String company,
            @Field("address") String address,
            @Field("id_user") String id_user);

    @FormUrlEncoded
    @POST(LOGOUT)
    Call<MessageModel> sendLogoutRequest(@Field("id_user") String id_user);

    @FormUrlEncoded
    @POST(MACHINE_INQUIRY)
    Call<MessageModel> sendInquiryRequest(@Field("id_user") String id_user, @Field("id_product") String id_product, @Field("comment") String comment);

//    @GET(PRODUCT_LIST)
    @FormUrlEncoded
    @POST(PRODUCT_LIST)
    Call<ProductList> sendProductListRequest(@Field("id_user") String id_user);

    @FormUrlEncoded
    @POST(ADD_COMPLAINTS)
    Call<MessageModel> sendComplaintsRequest(@Field("id_user") String id_user,
                                             @Field("id_product") String id_product,
                                             @Field("machine_number") String machine_number,
                                             @Field("common_complaints") String common_complaints,
                                             @Field("extra_complaint") String extra_complaint);

    @FormUrlEncoded
    @POST(GET_COMPLAINTS)
    Call<ComplaintsList> sendGetComplaintsRequest(@Field("id_user") String id_user);

    @GET(ABOUT_US)
    Call<AboutUs> sendAboutUsRequest();

    @FormUrlEncoded
    @POST(FEEDBACK)
    Call<MessageModel> sendFeedbackRequest(@Field("id_user") String id_user,
                                           @Field("id_complaints") String id_complaints,
                                           @Field("e_name") String e_name,
                                           @Field("actual_cause") String actual_cause,
                                           @Field("solution") String solution,
                                           @Field("future_note") String future_note,
                                           @Field("rating") float rating);

    @FormUrlEncoded
    @POST(USER_VALIDATION_FROM_SERVER)
    Call<UserValidationFromServer> sendUserValidationRequest(@Field("id_user") String id_user);

    @FormUrlEncoded
    @POST(RESET_PASSWORD)
    Call<ResetPassword> sendResetPasswordRequest(@Field("phone") String phone, @Field("password") String password);

    @FormUrlEncoded
    @POST(ACTIVE_USER)
    Call<UserActivation> sendUserActivationRequest(@Field("id_user") String id_user);
}
