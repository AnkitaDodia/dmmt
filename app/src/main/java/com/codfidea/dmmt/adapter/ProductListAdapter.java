//package com.codfidea.dmmt.adapter;
//
//import android.support.v7.widget.RecyclerView;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.Button;
//import android.widget.ProgressBar;
//import android.widget.RelativeLayout;
//import android.widget.TextView;
//
//import com.bumptech.glide.Glide;
//import com.bumptech.glide.load.resource.drawable.GlideDrawable;
//import com.bumptech.glide.request.RequestListener;
//import com.bumptech.glide.request.target.Target;
//import com.codfidea.dmmt.activity.DashboardActivity;
//import com.codfidea.dmmt.R;
//import com.codfidea.dmmt.model.ProductListData;
//import com.codfidea.dmmt.view.SquareImageView;
//
//import java.util.ArrayList;
//
//public class ProductListAdapter extends RecyclerView.Adapter<ProductListAdapter.MyViewHolder>
//{
//    ArrayList<ProductListData> mGetSellersDataList = new ArrayList<>();
//
//    DashboardActivity mContext;
//
//    public ProductListAdapter(DashboardActivity mContext, ArrayList<ProductListData> mList)
//    {
//        this.mContext = mContext;
//        this.mGetSellersDataList = mList;
//    }
//
//    @Override
//    public ProductListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        View itemView = LayoutInflater.from(parent.getContext())
//                .inflate(R.layout.row_item_product, null);
//
//        return new MyViewHolder(itemView);
//    }
//
//    @Override
//    public void onBindViewHolder(final ProductListAdapter.MyViewHolder holder, final int position) {
//        ProductListData mGetProductData = mGetSellersDataList.get(position);
//
//
//        Glide.with(mContext).load(mGetProductData.getProductImg())
//                .listener(new RequestListener<String, GlideDrawable>() {
//                    @Override
//                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
//                        holder.progress_product.setVisibility(View.GONE);
//                        return false;
//                    }
//
//                    @Override
//                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
////                        holder.img_vendors.setImageDrawable(resource);
//                        holder.progress_product.setVisibility(View.GONE);
//                        return false;
//                    }
//                }).into(holder.image_product);
//
//        holder.text_product_name.setText(mGetProductData.getProductName());
//    }
//
//    @Override
//    public int getItemCount() {
//        return mGetSellersDataList.size();
//    }
//
//    public class MyViewHolder extends RecyclerView.ViewHolder
//    {
//        RelativeLayout layout_product_row_main;
//
//        SquareImageView image_product;
//
//        TextView text_product_name;
//
//        ProgressBar progress_product;
//
//        Button btn_send_inquiry;
//
//        public MyViewHolder(View itemView)
//        {
//            super(itemView);
//
//            layout_product_row_main = itemView.findViewById(R.id.layout_product_row_main);
//
//            image_product = itemView.findViewById(R.id.image_product);
//
//            text_product_name = itemView.findViewById(R.id.text_product_name);
//
//            progress_product = itemView.findViewById(R.id.progress_product);
//
//            btn_send_inquiry = itemView.findViewById(R.id.btn_send_inquiry);
//
//            mContext.overrideFonts(layout_product_row_main,mContext);
//        }
//    }
//}
