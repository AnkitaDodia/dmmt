package com.codfidea.dmmt.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.codfidea.dmmt.R;
import com.codfidea.dmmt.model.ProductListData;
import com.codfidea.dmmt.view.SquareImageView;

import java.util.ArrayList;

public class ViewPagerAdapter extends PagerAdapter {
    // Declare Variables
    Context mContext;
    LayoutInflater inflater;
    ArrayList<ProductListData> mProductDataList = new ArrayList<>();

    public ViewPagerAdapter(Context context, ArrayList<ProductListData> mList) {
        this.mContext = context;
        this.mProductDataList = mList;
    }

    @Override
    public int getCount() {
        return mProductDataList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        // Declare Variables
        TextView txt_product_title;
        final ImageView img_product;
        final ProgressBar progress_product;

        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = inflater.inflate(R.layout.item_viewpager, container,
                false);

        txt_product_title = (TextView) itemView.findViewById(R.id.txt_product_title);
        txt_product_title.setText(mProductDataList.get(position).getProductName());
        img_product = (SquareImageView) itemView.findViewById(R.id.img_product);
        progress_product = itemView.findViewById(R.id.progress_product);

        Glide.with(mContext).load(mProductDataList.get(position).getProductImg())
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        progress_product.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
//                        img_product.setImageDrawable(resource);
                        progress_product.setVisibility(View.GONE);
                        return false;
                    }
                }).into(img_product);

        // Add viewpager_item.xml to ViewPager
        ((ViewPager) container).addView(itemView);

        return itemView;
    }



    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        // Remove viewpager_item.xml from ViewPager
        ((ViewPager) container).removeView((LinearLayout) object);

    }
}