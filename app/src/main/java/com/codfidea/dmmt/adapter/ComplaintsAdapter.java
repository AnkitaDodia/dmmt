package com.codfidea.dmmt.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.codfidea.dmmt.R;
import com.codfidea.dmmt.model.ComplaintsListData;

import java.util.ArrayList;

/**
 * Created by My 7 on 14-Aug-18.
 */

public class ComplaintsAdapter extends RecyclerView.Adapter<ComplaintsAdapter.MyViewHolder>
{
    ArrayList<ComplaintsListData> mComplaintsList= new ArrayList<>();
    Context mContext;


    public ComplaintsAdapter(Context mContext, ArrayList<ComplaintsListData> mList)
    {
        this.mContext = mContext;
        this.mComplaintsList = mList;

    }

    @Override
    public ComplaintsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_complaints, null);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ComplaintsAdapter.MyViewHolder holder, final int position) {
        ComplaintsListData mComplaintsListData = mComplaintsList.get(position);

        LinearLayout.LayoutParams params = new
                LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        holder.ll_row_complaints.setLayoutParams(params);



        holder.txt_row_complaint_name.setText(mComplaintsListData.getExtraComplaints());
//        holder.txt_row_complaint_info.setText(mComplaintsListData.getProductName());
        holder.txt_row_machine_name.setText(mComplaintsListData.getProductName());
        holder.txt_row_complaint_number.setText("કમ્પ્લેઇન આઈડી: "+mComplaintsListData.getIdDisplay());

        if(mComplaintsListData.getStatus() == 0){
            holder.img_complaint_status.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_on_progress));
        }else {
            holder.img_complaint_status.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_solved));
        }

    }

    @Override
    public int getItemCount() {
        return mComplaintsList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        LinearLayout ll_row_complaints;
        ImageView img_complaint_status;
        TextView txt_row_complaint_name, txt_row_machine_name,txt_row_complaint_number;
//        ProgressBar prgs_row_latest_news;

        public MyViewHolder(View itemView)
        {
            super(itemView);


            ll_row_complaints = itemView.findViewById(R.id.ll_row_complaints);
            img_complaint_status = itemView.findViewById(R.id.img_complaint_status);
            txt_row_complaint_name = itemView.findViewById(R.id.txt_row_complaint_name);
            txt_row_machine_name = itemView.findViewById(R.id.txt_row_machine_name);
            txt_row_complaint_number = itemView.findViewById(R.id.txt_row_complaint_number);

//            mContext.overrideFonts(ll_row_parent_latest_news, mContext);
        }

    }
}
