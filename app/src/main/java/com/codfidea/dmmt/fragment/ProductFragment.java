package com.codfidea.dmmt.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.codfidea.dmmt.R;
import com.codfidea.dmmt.activity.HomeActivity;
import com.codfidea.dmmt.activity.LoginActivity;
import com.codfidea.dmmt.activity.SendInquiryActivity;
import com.codfidea.dmmt.adapter.ViewPagerAdapter;
import com.codfidea.dmmt.common.BaseActivity;
import com.codfidea.dmmt.model.ProductList;
import com.codfidea.dmmt.model.ProductListData;
import com.codfidea.dmmt.model.UserDetails;
import com.codfidea.dmmt.restinterface.RestInterface;
import com.google.gson.Gson;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class ProductFragment extends Fragment {

    HomeActivity mContext;
    ViewPager vp_product;
    LinearLayout ll_viewpager, ll_empty_view;
    TextView txt_empty_products;

    private int mPosition;
    private Button btn_website, btn_inquiry;

    public ProductFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_product, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mContext = (HomeActivity) getActivity();

        initViews(view);
        SetClickListner();

        if (BaseActivity.CheckInternet(mContext)) {
            sendProductRequest();
        } else {
            Toast.makeText(mContext, "કોઈ ઇન્ટરનેટ કનેક્શન મળ્યું નથી.", Toast.LENGTH_SHORT).show();
        }

    }

    private void initViews(View view) {

        vp_product = view.findViewById(R.id.vp_product);
        btn_website = view.findViewById(R.id.btn_website);
        btn_inquiry = view.findViewById(R.id.btn_inquiry);

        ll_empty_view = view.findViewById(R.id.ll_empty_view);
        ll_viewpager = view.findViewById(R.id.ll_viewpager);
        txt_empty_products = view.findViewById(R.id.txt_empty_products);

    }

    private void SetClickListner() {

        vp_product.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            public void onPageScrollStateChanged(int state) {
            }

            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                mPosition = position;
            }

            public void onPageSelected(int position) {
                // Check if this is the page you want.
            }
        });

        btn_website.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (BaseActivity.mProductDataList.get(mPosition).getWebsite() == null) {

//                    Toast.makeText(mContext, "કોઈ ઇન્ટરનેટ કનેક્શન મળ્યું નથી.", Toast.LENGTH_SHORT).show();

                } else {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(BaseActivity.mProductDataList.get(mPosition).getWebsite())));
                }

            }
        });

        btn_inquiry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                BaseActivity.INQUIRY_MACHINE_NAME = "" + BaseActivity.mProductDataList.get(mPosition).getProductName();
                BaseActivity.INQUIRY_MACHINE_ID = "" + BaseActivity.mProductDataList.get(mPosition).getIdProduct();

                startActivity(new Intent(mContext, SendInquiryActivity.class));
            }
        });

    }


    private void sendProductRequest() {
        mContext.showWaitIndicator(true);

        UserDetails mUserDetails = BaseActivity.getUserDetails(mContext);

        Log.e("PRODUCT_FRAGMENT","IdUser : "+mUserDetails.getIdUser());
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RestInterface.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestInterface service = retrofit.create(RestInterface.class);

        Call<ProductList> call = service.sendProductListRequest(mUserDetails.getIdUser());

        call.enqueue(new Callback<ProductList>() {
            @Override
            public void onResponse(Call<ProductList> call, Response<ProductList> response) {

                mContext.showWaitIndicator(false);

                Log.e("PRODUCT_LIST", "CODE: " + response.code());

                try {
                    if (response.code() == 200) {

                        Log.e("PRODUCT_LIST", "FULL RESPONSE : " + new Gson().toJson(response.body().getData()));
                        Log.e("PRODUCT_LIST", "STATUS: " + response.body().getStatus());

                        if(response.body().getUserStatus().equalsIgnoreCase("0")){
                            mContext.setUserStatus(0);
                        }else{
                            mContext.setUserStatus(1);
                        }



                        if(response.body().getUserStatus().equalsIgnoreCase("0")){
                            mContext.setUserStatus(0);
                        }else{
                            mContext.setUserStatus(1);
                        }


                        if (response.body().getStatus() == 1) {
                            ll_viewpager.setVisibility(View.VISIBLE);
                            ll_empty_view.setVisibility(View.GONE);
                            BaseActivity.mProductDataList.clear();
                            BaseActivity.mProductDataList = response.body().getData();
                            ViewPagerAdapter mViewPagerAdapter = new ViewPagerAdapter(mContext, BaseActivity.mProductDataList);
                            vp_product.setAdapter(mViewPagerAdapter);

                        } else if (response.body().getStatus() == 0) {

                            ll_viewpager.setVisibility(View.GONE);
                            ll_empty_view.setVisibility(View.VISIBLE);
//                            rv_products.setVisibility(View.GONE);
//                            txt_empty_products.setVisibility(View.VISIBLE);
                            txt_empty_products.setText(" "+response.body().getMessage());
                        }
                    }
                } catch (Exception e) {
                    mContext.showWaitIndicator(false);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ProductList> call, Throwable t) {
                mContext.showWaitIndicator(false);
                Log.e("onFailure", t.toString());
                Toast.makeText(mContext, " " + mContext.getString(R.string.network_calling_error_msg), Toast.LENGTH_SHORT).show();
            }
        });
    }




}
