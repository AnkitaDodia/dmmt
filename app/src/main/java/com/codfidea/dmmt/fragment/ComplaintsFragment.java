package com.codfidea.dmmt.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;
import com.codfidea.dmmt.R;
import com.codfidea.dmmt.activity.HomeActivity;
import com.codfidea.dmmt.common.BaseActivity;
import com.codfidea.dmmt.model.MessageModel;
import com.codfidea.dmmt.model.UserActivation;
import com.codfidea.dmmt.model.UserDetails;
import com.codfidea.dmmt.restinterface.RestInterface;
import com.google.gson.Gson;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class ComplaintsFragment extends Fragment {

    final String TAG = "COMPLAINTS";
    HomeActivity mContext;

    int selectedItem;
    Spinner sp_machine_names;
    TextInputEditText edt_machine_number, edt_comlaints;
    Button btn_complaint;
    LinearLayout ll_complain_main;

    public ComplaintsFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_complaints, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mContext = (HomeActivity) getActivity();

        InitViews(view);
        SetClickListner();

        if (BaseActivity.CheckInternet(mContext)) {
            sendUserActivationRequest();
        } else {
            Toast.makeText(mContext, "કોઈ ઇન્ટરનેટ કનેક્શન મળ્યું નથી.", Toast.LENGTH_SHORT).show();
        }
    }

    private void InitViews(View view) {
        ll_complain_main = view.findViewById(R.id.ll_complain_main);
        mContext.overrideFonts(ll_complain_main,mContext);
        ll_complain_main.setVisibility(View.GONE);

        edt_machine_number = view.findViewById(R.id.edt_machine_number);
        edt_comlaints = view.findViewById(R.id.edt_comlaints);

        btn_complaint = view.findViewById(R.id.btn_complaint);

        sp_machine_names = view.findViewById(R.id.sp_machine_names);
        setSpinnerAdapter();

    }
    private void SetClickListner(){

        btn_complaint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (TextUtils.isEmpty(edt_machine_number.getText().toString().trim())) {
//                    edt_login_email.setError("Please enter email");
                    Toast.makeText(mContext,"આખું નામ એન્ટર કરો.",Toast.LENGTH_LONG).show();
                } else if (TextUtils.isEmpty(edt_comlaints.getText().toString().trim())) {
//                    edt_login_email.setError("Please enter email");
                    Toast.makeText(mContext,"કંપની નું નામ એન્ટર કરો.",Toast.LENGTH_LONG).show();
                } else {
                    if (BaseActivity.CheckInternet(mContext)) {
                        sendComplaintsRequest();
                    } else {
                        Toast.makeText(mContext, "કોઈ ઇન્ટરનેટ કનેક્શન મળ્યું નથી.", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });


        sp_machine_names.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                selectedItem = i;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }


    private void setSpinnerAdapter()
    {

//        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, sItemList); //selected item will look like a spinner set from XML
//        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        simple_dropdown_item_1line

        FillMachineNameList();

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, BaseActivity.mMachineNameList) {

            @Override
            public View getDropDownView(int position, View convertView, ViewGroup parent)
            {
                View v = null;
                v = super.getDropDownView(position, null, parent);
                // If this is the selected item position
//                if (position == selectedItem) {
//                    v.setBackgroundColor(Color.LTGRAY);
//                }
//                else {
//                    // for other views
//                    v.setBackgroundColor(Color.WHITE);
//
//                }
                return v;
            }
        };

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);

        // attaching data adapter to spinner
        sp_machine_names.setAdapter(dataAdapter);
        sp_machine_names.setSelection(0,true);
    }


    private void FillMachineNameList() {

        BaseActivity.mMachineNameList.clear();
        for (int i = 0; i < BaseActivity.mProductDataList.size(); i++) {

            BaseActivity.mMachineNameList.add(BaseActivity.mProductDataList.get(i).getProductShortName());
        }
    }

    //Code for checking if user is activated from server or not
    private void sendUserActivationRequest() {

        mContext.showWaitIndicator(true);

        UserDetails mUserDetails = mContext.getUserDetails(mContext);

        Log.e(TAG,"IdUser : "+mUserDetails.getIdUser());

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RestInterface.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestInterface service = retrofit.create(RestInterface.class);

        Call<UserActivation> call = service.sendUserActivationRequest(mUserDetails.getIdUser());

        call.enqueue(new Callback<UserActivation>() {
            @Override
            public void onResponse(Call<UserActivation> call, Response<UserActivation> response) {

                mContext.showWaitIndicator(false);

                try {
                    if (response.code() == 200) {

                        Log.e("USER_ACTIVATION", "FULL RESPONSE : "+ new Gson().toJson(response.body().getData()));

                        if(response.body().getUserStatus().equalsIgnoreCase("1")){
                            ll_complain_main.setVisibility(View.VISIBLE);
                        }else {
                            showNoComplaintsAlert();
                        }
                    }

                } catch (Exception e) {
//                    Log.d("onResponse", "There is an error");
                    mContext.showWaitIndicator(false);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<UserActivation> call, Throwable t) {
                mContext.showWaitIndicator(false);
                Log.d("onFailure", t.toString());
                Toast.makeText(mContext," "+mContext.getString(R.string.network_calling_error_msg), Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void showNoComplaintsAlert() {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(mContext);
        builder.setTitle("માફ કરજો !")
                .setMessage("તમે કમ્પ્લેઇન નહિ કરી શકો, કમ્પ્લેઇનસ કરવા તમારા એકાઉન્ટ ને વેરીફાય કરવો.")
                .setPositiveButton("ઓકે", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        mContext.backtoproduct();
                    }
                })
                .show();
    }

    //Code for sending complaint to server
    private void sendComplaintsRequest() {

        mContext.showWaitIndicator(true);

        UserDetails mUserDetails = mContext.getUserDetails(mContext);

        String IdUser = mUserDetails.getIdUser();
        String IdProduct = BaseActivity.mProductDataList.get(selectedItem).getIdProduct();
        String MachineNumber = edt_machine_number.getText().toString().trim();
        String Complaint = edt_comlaints.getText().toString().trim();


        Log.e(TAG,"IdUser : "+IdUser);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RestInterface.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        RestInterface service = retrofit.create(RestInterface.class);

        Call<MessageModel> call = service.sendComplaintsRequest(IdUser, IdProduct, MachineNumber, null, Complaint);

        call.enqueue(new Callback<MessageModel>() {
            @Override
            public void onResponse(Call<MessageModel> call, Response<MessageModel> response) {

                mContext.showWaitIndicator(false);

                try {
                    if (response.code() == 200) {

                        Log.e(TAG,"RESPONSE : "+response.body().getMessage());
//                        Toast.makeText(mContext, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();

                        if(response.body().getStatus() == 1){

                            showThankYouAlert();

                        }else {

                        }
                    }
                } catch (Exception e) {
//                    Log.d("onResponse", "There is an error");
                    mContext.showWaitIndicator(false);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<MessageModel> call, Throwable t) {
                mContext.showWaitIndicator(false);
                Log.d(TAG,"onFailure"+t.toString());
                Toast.makeText(mContext," "+mContext.getString(R.string.network_calling_error_msg), Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void showThankYouAlert() {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(mContext);
        builder.setTitle("આભાર !")
                .setMessage("કમ્પ્લેઇન નોંધવા બદલ આભાર, થોડા સમય માં અમે તમારો સંપર્ક કરીશું.")
                .setPositiveButton("ઓકે", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        mContext.backtoproduct();
                    }
                })
//                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int which) {
//
//                    }
//                })
                .show();
    }

}
