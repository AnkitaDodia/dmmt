package com.codfidea.dmmt.fragment;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Toast;

import com.codfidea.dmmt.R;
import com.codfidea.dmmt.activity.HomeActivity;
import com.codfidea.dmmt.adapter.ViewPagerAdapter;
import com.codfidea.dmmt.common.BaseActivity;
import com.codfidea.dmmt.model.AboutUs;
import com.codfidea.dmmt.model.ProductList;
import com.codfidea.dmmt.restinterface.RestInterface;
import com.google.gson.Gson;

import java.io.InputStream;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class AboutUsFragment extends Fragment {

    HomeActivity mContext;
    WebView webview_content;
    private String mAboutUs;

    public AboutUsFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_aboutus, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mContext = (HomeActivity) getActivity();

        initViews(view);

        if (BaseActivity.CheckInternet(mContext)) {
            sendAboutUsRequest();
        } else {
            Toast.makeText(mContext, "કોઈ ઇન્ટરનેટ કનેક્શન મળ્યું નથી.", Toast.LENGTH_SHORT).show();
        }

    }

    private void initViews(View view) {

        webview_content = view.findViewById(R.id.webview_content);
    }

    private void sendAboutUsRequest() {
        mContext.showWaitIndicator(true);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RestInterface.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestInterface service = retrofit.create(RestInterface.class);

        Call<AboutUs> call = service.sendAboutUsRequest();

        call.enqueue(new Callback<AboutUs>() {
            @Override
            public void onResponse(Call<AboutUs> call, Response<AboutUs> response) {

                mContext.showWaitIndicator(false);

                try {
                    if (response.code() == 200) {

                        Log.e("ABOUT_US", "FULL RESPONSE : " + new Gson().toJson(response.body().getData()));

                        if (response.body().getStatus() == 1) {

                            mAboutUs = response.body().getData();
                            SetAboutUsData(mAboutUs);
                        }
                    }
                } catch (Exception e) {
                    mContext.showWaitIndicator(false);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<AboutUs> call, Throwable t) {
                mContext.showWaitIndicator(false);
                Log.d("onFailure", t.toString());
                Toast.makeText(mContext, " " + mContext.getString(R.string.network_calling_error_msg), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void SetAboutUsData(String aboutus) {

        try {

            String text = "<html><body style=\"color:#484848;font-family:file:///android_asset//SHRUTI.TTF;font-size:18px;\"'background-color:transparent' >" + "<p align=\"justify\">" + aboutus + "</p>" + "</body></html>";

//            String all="<html><body>"+text.replace("\r\n", "<br/>")+"</body></html>";

            webview_content.loadData("<font>" + text + "</font>",
                    "text/html; charset=UTF-8", null);


        } catch (Exception e) {

//            Toast.makeText(ctx, ""+e, Toast.LENGTH_LONG).show();

        }

    }

}
