package com.codfidea.dmmt.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.codfidea.dmmt.R;
import com.codfidea.dmmt.activity.FeedBackFormActivity;
import com.codfidea.dmmt.activity.HomeActivity;
import com.codfidea.dmmt.adapter.ComplaintsAdapter;
import com.codfidea.dmmt.common.BaseActivity;
import com.codfidea.dmmt.model.ComplaintsList;
import com.codfidea.dmmt.model.ComplaintsListData;
import com.codfidea.dmmt.model.UserDetails;
import com.codfidea.dmmt.restinterface.RestInterface;
import com.google.gson.Gson;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class FeedBackFragment extends Fragment {

    final String TAG = "COMPLAINTS_LIST";
    HomeActivity mContext;
    RecyclerView rv_complaints;
    TextView txt_empty_complaints;

    public FeedBackFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_feedback, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mContext = (HomeActivity) getActivity();

        initViews(view);
        SetClickListner();

        if (BaseActivity.CheckInternet(mContext)) {
            sendGetComplaintsRequest();
        } else {
            Toast.makeText(mContext, "કોઈ ઇન્ટરનેટ કનેક્શન મળ્યું નથી.", Toast.LENGTH_SHORT).show();
        }
    }

    private void initViews(View view) {

        txt_empty_complaints = view.findViewById(R.id.txt_empty_complaints);
        rv_complaints = view.findViewById(R.id.rv_complaints);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(mContext);
        rv_complaints.setLayoutManager(mLayoutManager);
//        rv_packagelist.addItemDecoration(new DividerItemDecoration(mContext,
//                DividerItemDecoration.VERTICAL));

        DividerItemDecoration itemDecorator = new DividerItemDecoration(mContext, DividerItemDecoration.VERTICAL);
        itemDecorator.setDrawable(ContextCompat.getDrawable(mContext, R.drawable.rv_divider));
        rv_complaints.addItemDecoration(itemDecorator);

    }
    private void SetClickListner(){

        rv_complaints.addOnItemTouchListener(new BaseActivity.RecyclerTouchListener(mContext, rv_complaints, new BaseActivity.ClickListener() {
            @Override
            public void onClick(View view, int position) {

                if(BaseActivity.mComplaintsList.get(position).getStatus() == 1){
                    Toast.makeText(mContext,"અમે આ ફરિયાદને સોલ્વ કરી દીધી છે",Toast.LENGTH_LONG).show();
                }else {

                    BaseActivity.COMPLAINT_POSITION = position;
                    startActivity(new Intent(mContext, FeedBackFormActivity.class));
                }
            }

            @Override
            public void onLongClick(View view, int position) {
            }
        }));
    }

    private void sendGetComplaintsRequest() {

        mContext.showWaitIndicator(true);

        UserDetails mUserDetails = mContext.getUserDetails(mContext);

        Log.e(TAG,"IdUser : "+mUserDetails.getIdUser());

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RestInterface.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestInterface service = retrofit.create(RestInterface.class);

        Call<ComplaintsList> call = service.sendGetComplaintsRequest(mUserDetails.getIdUser());

        call.enqueue(new Callback<ComplaintsList>() {
            @Override
            public void onResponse(Call<ComplaintsList> call, Response<ComplaintsList> response) {

                mContext.showWaitIndicator(false);

                try {
                    if (response.code() == 200) {

                        Log.e("COMPLAINTS_LIST", "FULL RESPONSE : "+ new Gson().toJson(response.body().getData()));

                        if(response.body().getStatus() == 1){

                            rv_complaints.setVisibility(View.VISIBLE);
                            txt_empty_complaints.setVisibility(View.GONE);

                            BaseActivity.mComplaintsList = response.body().getData();
                            setAdapter(BaseActivity.mComplaintsList);

                        }else {
                            rv_complaints.setVisibility(View.GONE);
                            txt_empty_complaints.setVisibility(View.VISIBLE);
                            txt_empty_complaints.setText("No Complains Available");
                        }
                    }

                } catch (Exception e) {
//                    Log.d("onResponse", "There is an error");
                    mContext.showWaitIndicator(false);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ComplaintsList> call, Throwable t) {
                mContext.showWaitIndicator(false);
                Log.d("onFailure", t.toString());
                Toast.makeText(mContext," "+mContext.getString(R.string.network_calling_error_msg), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setAdapter(ArrayList<ComplaintsListData> mComplaintsListData){

        ComplaintsAdapter mComplaintsAdapter = new ComplaintsAdapter(mContext, mComplaintsListData);
        rv_complaints.setAdapter(mComplaintsAdapter);
    }
}
