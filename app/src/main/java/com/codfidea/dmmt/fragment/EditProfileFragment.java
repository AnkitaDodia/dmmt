package com.codfidea.dmmt.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.codfidea.dmmt.R;
import com.codfidea.dmmt.activity.HomeActivity;
import com.codfidea.dmmt.activity.LoginActivity;
import com.codfidea.dmmt.activity.RegistrationActivity;
import com.codfidea.dmmt.common.BaseActivity;
import com.codfidea.dmmt.model.SignUp;
import com.codfidea.dmmt.model.UserDetails;
import com.codfidea.dmmt.restinterface.RestInterface;
import com.google.gson.Gson;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class EditProfileFragment extends Fragment {

    final String TAG = "EDIT_PROFILE";
    HomeActivity mContext;
    LinearLayout ll_parent_update;
    TextInputEditText edt_reg_fullname, edt_reg_company_name, edt_reg_phone, edt_reg_email, edt_reg_address;
    Button btn_update;

    public EditProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_edit_profile, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mContext = (HomeActivity) getActivity();

        initViews(view);
        SetClickListner();
        SetData();

    }

    private void SetData() {

        UserDetails mUserDetails = mContext.getUserDetails(mContext);

        edt_reg_fullname.setText(mUserDetails.getFirstName());
        edt_reg_company_name.setText(mUserDetails.getCompany());
        edt_reg_phone.setText(mUserDetails.getPhonenumber());
        edt_reg_email.setText(mUserDetails.getEmail());
        edt_reg_address.setText(mUserDetails.getAddress());

        Log.e(TAG,"IdUser : "+mUserDetails.getIdUser());

    }

    private void initViews(View view) {

        ll_parent_update = view.findViewById(R.id.ll_parent_update);

        edt_reg_fullname = view.findViewById(R.id.edt_reg_fullname);
        edt_reg_company_name = view.findViewById(R.id.edt_reg_company_name);
        edt_reg_phone = view.findViewById(R.id.edt_reg_phone);
        edt_reg_email = view.findViewById(R.id.edt_reg_email);
        edt_reg_address = view.findViewById(R.id.edt_reg_address);

        btn_update = view.findViewById(R.id.btn_update);

        mContext.overrideFonts(ll_parent_update, mContext);

    }

    private void SetClickListner(){

        btn_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (TextUtils.isEmpty(edt_reg_fullname.getText().toString().trim())) {
//                    edt_login_email.setError("Please enter email");
                    Toast.makeText(mContext,"આખું નામ એન્ટર કરો.",Toast.LENGTH_LONG).show();
                } else if (TextUtils.isEmpty(edt_reg_company_name.getText().toString().trim())) {
//                    edt_login_email.setError("Please enter email");
                    Toast.makeText(mContext,"કંપની નું નામ એન્ટર કરો.",Toast.LENGTH_LONG).show();
                } else if (TextUtils.isEmpty(edt_reg_phone.getText().toString().trim())) {
//                    edt_login_email.setError("Please enter email");
                    Toast.makeText(mContext,"ફોન નંબર એન્ટર કરો.",Toast.LENGTH_LONG).show();
                }
//                else if (TextUtils.isEmpty(edt_reg_email.getText().toString().trim())) {
////                    edt_login_email.setError("Please enter email");
//                    Toast.makeText(mContext,"ઇમેઇલ એડ્રેસ એન્ટર કરો.",Toast.LENGTH_LONG).show();
//                } else if (!BaseActivity.isValidEmail(edt_reg_email.getText().toString())) {
////                    edt_login_email.setError("Please enter valid email");
//                    Toast.makeText(mContext,"સાચું ઇમેઇલ એડ્રેસ એન્ટર કરો.",Toast.LENGTH_LONG).show();
//                }
                else if (TextUtils.isEmpty(edt_reg_address.getText().toString().trim())) {
//                    edt_login_password.setError("Please enter valid password");
                    Toast.makeText(mContext,"સરનામું એડ્રેસ એન્ટર કરો.",Toast.LENGTH_LONG).show();
                } else {
                    if (BaseActivity.CheckInternet(mContext)) {
                        sendEditProfileRequest();
                    } else {
                        Toast.makeText(mContext, "કોઈ ઇન્ટરનેટ કનેક્શન મળ્યું નથી.", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

    }

    private void sendEditProfileRequest() {

        mContext.showWaitIndicator(true);

        String fullname = edt_reg_fullname.getText().toString().trim();
        String company_name = edt_reg_company_name.getText().toString().trim();
        String phone = edt_reg_phone.getText().toString().trim();
        String address = edt_reg_address.getText().toString().trim();
        String email = edt_reg_email.getText().toString().trim();

        UserDetails mUserDetails = mContext.getUserDetails(mContext);

        Log.e(TAG,"IdUser : "+mUserDetails.getIdUser());
        Log.e(TAG,"fullname : "+fullname);
        Log.e(TAG,"company_name : "+company_name);
        Log.e(TAG,"phone : "+phone);
        Log.e(TAG,"address : "+address);
        Log.e(TAG,"Email : "+email);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RestInterface.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        RestInterface service = retrofit.create(RestInterface.class);

        Call<SignUp> call = service.sendEditProfileRequest(fullname, email, phone, company_name, address, mUserDetails.getIdUser());

        call.enqueue(new Callback<SignUp>() {
            @Override
            public void onResponse(Call<SignUp> call, Response<SignUp> response) {

                mContext.showWaitIndicator(false);

                try {
                    if (response.code() == 200) {
                        Log.e(TAG, "FULL RESPONSE : "+ new Gson().toJson(response.body().getData()));
                        Log.e(TAG,"RESPONSE : "+response.body().getMessage());
                        Toast.makeText(mContext, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();

                        if(response.body().getStatus() == 1){
                            Log.e(TAG,"RESPONSE : "+response.body().getData().getPhone());


                            UserDetails mUserDetails = new UserDetails();

                            mUserDetails.setIdUser(response.body().getData().getIdUser());
                            mUserDetails.setFirstName(response.body().getData().getName());
                            mUserDetails.setAddress(response.body().getData().getAddress());
                            mUserDetails.setEmail(response.body().getData().getEmail());
                            mUserDetails.setCompany(response.body().getData().getCompany());
                            mUserDetails.setPassword(response.body().getData().getPassword());
                            mUserDetails.setPhonenumber(response.body().getData().getPhone());

                            if(response.body().getData().getActivated().equalsIgnoreCase("0")){
                                mUserDetails.setActivated(false);
                                mContext.setUserStatus(0);
                            }else{
                                mUserDetails.setActivated(true);
                                mContext.setUserStatus(1);
                            }

                            mContext.setUserDetails(mContext, mUserDetails);
                            mContext.backtoproduct();

                        }else {

                        }
                    }
                } catch (Exception e) {
//                    Log.d("onResponse", "There is an error");
                    mContext.showWaitIndicator(false);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<SignUp> call, Throwable t) {
                mContext.showWaitIndicator(false);
                Log.d(TAG,"onFailure"+t.toString());
                Toast.makeText(mContext," "+mContext.getString(R.string.network_calling_error_msg), Toast.LENGTH_SHORT).show();

            }
        });
    }

}
