package com.codfidea.dmmt.common;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.codfidea.dmmt.R;
import com.codfidea.dmmt.model.ComplaintsListData;
import com.codfidea.dmmt.model.ProductListData;
import com.codfidea.dmmt.model.UserDetails;
import com.codfidea.dmmt.utility.InternetStatus;
import com.codfidea.dmmt.utility.TypeFaces;

import java.util.ArrayList;

public class BaseActivity extends AppCompatActivity
{
    ProgressDialog mProgressDialog;
    private static final String PREFS_NAME = "DMMT_LOCAL_DB";
    private static final String DEFAULT_VAL = null;

    public static String INQUIRY_MACHINE_NAME = null;
    public static String INQUIRY_MACHINE_ID = null;
    public static int COMPLAINT_POSITION = 0;

    public static ArrayList<String> mMachineNameList = new ArrayList<String>();
    public static ArrayList<ProductListData> mProductDataList = new ArrayList<>();
    public static ArrayList<ComplaintsListData> mComplaintsList = new ArrayList<>();


    public static String FCMTOKEN = null;


    public static boolean CheckInternet(Context mContext){

        return new InternetStatus().isInternetOn(mContext);
    }


    public void showWaitIndicator(boolean state) {
//        showWaitIndicator(state, "");

        if (state) {

            showProgressDialog();

        } else {

            dismissProgressDialog();
        }
    }

    public void showProgressDialog() {
        if (mProgressDialog == null) {
            initProgressDialog();
        }
        if (mProgressDialog != null && !mProgressDialog.isShowing()) {
//            mProgressDialog.setMessage("LOADING...");
            mProgressDialog.show();
            ProgressBar progressbar=(ProgressBar)mProgressDialog.findViewById(android.R.id.progress);
            progressbar.getIndeterminateDrawable().setColorFilter(Color.parseColor("#005ca1"), android.graphics.PorterDuff.Mode.SRC_IN);
        }
    }

    private void initProgressDialog() {
        Log.d("ProgressBar", "initProgressDialog()");
        mProgressDialog = new ProgressDialog(this,  R.style.TransparentProgressDialog);
//        mProgressDialog.setMessage(LOADING);
        mProgressDialog.setCancelable(false);
//        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Large);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setCanceledOnTouchOutside(false);
    }

    public void dismissProgressDialog() {
        Log.d("ProgressBar", "dismissProgressDialog()");
        try {
            if (mProgressDialog != null) {
                mProgressDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Code for get and set login
    public void setLogin(int i)
    {
        SharedPreferences sp = getSharedPreferences("LOGIN",MODE_PRIVATE);
        SharedPreferences.Editor spe = sp.edit();
        spe.clear();
        spe.putInt("Login",i);
        spe.apply();
    }

    public int getLogin()
    {
        SharedPreferences sp = getSharedPreferences("LOGIN",MODE_PRIVATE);
        int i = sp.getInt("Login",0);
        return i;
    }
    // Code for get and set login

    // Code for get and set login
    public void setUserStatus(int i)
    {
        SharedPreferences sp = getSharedPreferences("USER_STATUS",MODE_PRIVATE);
        SharedPreferences.Editor spe = sp.edit();
        spe.putInt("UserStatus",i);
        spe.apply();
    }

    public int getUserStatus()
    {
        SharedPreferences sp = getSharedPreferences("USER_STATUS",MODE_PRIVATE);
        int i = sp.getInt("UserStatus",0);
        return i;
    }
    // Code for get and set login

    public Typeface getGujaratiFonts(Context mContext)
    {
        Typeface font = TypeFaces.getTypeFace(mContext, "SHRUTI.TTF");
        return font;
    }

    public void overrideFonts(final View v, Context ctx) {
        try {
            if (v instanceof ViewGroup) {
                ViewGroup vg = (ViewGroup) v;
                for (int i = 0; i < vg.getChildCount(); i++) {
                    View child = vg.getChildAt(i);
                    overrideFonts(child, ctx);
                }
            } else if (v instanceof TextView) {
                ((TextView) v).setTypeface(getGujaratiFonts(ctx));
            }else if (v instanceof Button) {
                ((Button) v).setTypeface(getGujaratiFonts(ctx));
            }else if (v instanceof EditText) {
                ((EditText) v).setTypeface(getGujaratiFonts(ctx));
            }else if(v instanceof TextInputLayout) {
                ((TextInputLayout) v).setTypeface(getGujaratiFonts(ctx));
            }
        } catch (Exception e) {
        }
    }

    public final static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    public static void setUserDetails(Context context, UserDetails user) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor dbEditor = prefs.edit();

        dbEditor.putString("firstname",user.getFirstName());
        dbEditor.putString("lastname",user.getLastName());
        dbEditor.putString("email",user.getEmail());
        dbEditor.putString("company",user.getCompany());
        dbEditor.putString("address",user.getAddress());
        dbEditor.putString("userid",user.getIdUser());
        dbEditor.putString("phone",user.getPhonenumber());
        dbEditor.putBoolean("activated",user.getActivated());

//		dbEditor.apply();

        dbEditor.commit();
    }

    public static UserDetails getUserDetails(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);

        UserDetails user = new UserDetails();

        user.setFirstName(prefs.getString("firstname",DEFAULT_VAL));
        user.setLastName(prefs.getString("lastname",DEFAULT_VAL));
        user.setEmail(prefs.getString("email",DEFAULT_VAL));
        user.setCompany(prefs.getString("company",DEFAULT_VAL));
        user.setAddress(prefs.getString("address",DEFAULT_VAL));
        user.setIdUser(prefs.getString("userid",DEFAULT_VAL));
        user.setPhonenumber(prefs.getString("phone",DEFAULT_VAL));
        user.setActivated(prefs.getBoolean("activated",false));

        return user;
    }

    public static void ClearUserDetails(Context context) {

        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor dbEditor = prefs.edit();
        dbEditor.clear();
        dbEditor.commit();
    }

    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }

    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}
